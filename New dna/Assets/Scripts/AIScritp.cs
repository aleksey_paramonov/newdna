﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Simple implementation of AI for the game.
/// </summary>
public class AIScritp : Player
{
    private CellGrid _cellGrid;
    private System.Random _rnd;
    public Cell DestinationCell;
    public Cell StartCell;
    public List<Cell> Mpath;
    public List<Cell> path;
    public List<Cell> Testpath;
    public Unit unit;
    
    
    //public Cell StartCell;
    
    public AIScritp()
    {
        _rnd = new System.Random();
    }
    public void GetNewPath(){
        if(unit.Cell==DestinationCell){
             Cell swap=DestinationCell;
             DestinationCell=StartCell;
             StartCell=swap;
        }
         Mpath = unit.FindPath(_cellGrid.Cells, DestinationCell);
        Mpath.Reverse();
        path=new List<Cell>(Mpath);
    }
    public override void Play(CellGrid cellGrid)
    {
        cellGrid.CellGridState = new CellGridStateAiTurn(cellGrid);
        _cellGrid = cellGrid;
        
        GetNewPath();
        //path=Mpath;
        StartCoroutine(Play()); //Coroutine is necessary to allow Unity to run updates on other objects (like UI).
                                //Implementing this with threads would require a lot of modifications in other classes, as Unity API is not thread safe.
    }
    private IEnumerator Play()
    {
        
            // unit.MyMove(DestinationCell, path);
            // yield return 0;  
            if(path.Count>0){
                //Testpath.RemoveAll(Testpath);
                List<Cell>CurrentPath=new List<Cell>();
                Testpath=CurrentPath;
                //CurrentPath.Add(path[1]);
                CurrentPath.Add(path[0]);
                
                unit.MyMove(path[0], CurrentPath);
                path.Remove(unit.Cell);
                while (unit.isMoving)
                        yield return 0;
                GetNewPath();
                if(path.Count>0)
                   unit.ShowNextDirection(path[0]);
                
            }
            // else{
            //     Cell swap=DestinationCell;
            //     DestinationCell=StartCell;
            //     StartCell=swap;
                
            // }
                    
                   
                    
                  
                
        //If the path cost is greater than unit movement points, move as far as possible.
         
        _cellGrid.EndTurn(); 
        //yield return 0;    
    }
}