﻿using UnityEngine;
using System.Collections;

public class ActivateMainTexture : MonoBehaviour {

	// Use this for initialization
	public GameObject DnaImage;
	public GameObject DnaShadow;
	void Start () {
	
	}
	public void Activate(){
		DnaImage.SetActive(true);
		DnaShadow.SetActive(true);
	}
	// Update is called once per frame
	void Update () {
	
	}
}
