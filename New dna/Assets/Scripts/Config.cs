﻿using System;

public static class Config
{
	// параметры доступа к серверу

	public const string kConnectionString = "tcp://10.1.10.17:1883";
	//public const string kConnectionString = "tcp://10.10.50.177:1883";
	//public const string kConnectionString = "tcp://192.168.1.105:1883";
	

	//public const string kConnectionString = "tcp://maniac.server.local:1883";
	//public const string kConnectionString = "tcp://10.10.50.177:1883";


	public const string kDNALoadLevelTopic = "ufo/Bridge_DNA_LoadLevel/commands";
	public const string kDNAInfoScreensTopic= "ufo/Bridge_DNA_Screens/commands";
	
	public const string kDNAButtonsTopic = "ufo/Bridge_DNA_KEYPAD/state";
	
	
	public const string kDNAResetTopic = "ufo/Bridge_DNA/commands";
	public const string kDNAStateTopic = "ufo/Bridge_DNA/state";
	public const string kDNALevelStateTopic = "ufo/Bridge_DNA_Level/state";
	
	
	



	public const string kSimonRESET = "RESET";
    
}

