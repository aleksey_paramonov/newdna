﻿using UnityEngine;
using System.Collections;
using MqttLib;
using SimpleJSON;

public class MQTTHandlerBase : MonoBehaviour
{
	
	protected IMqtt client;
	public bool ClientConnection=false;
	protected virtual string clientId { get { return Random.RandomRange(1,99999).ToString(); } }
	
	void SendAliveMessage(){
		
		try{
			client.Publish("mqtt/state","Alive", QoS.OnceAndOnceOnly, false);
            
		}
		catch{
			client.Disconnect();
            ClientConnection=true;
		}
		
	}
	
	public virtual void Awake()
	{
		client = MqttClientFactory.CreateClient(Config.kConnectionString, clientId);
		client.Connected += new ConnectionDelegate(OnConnect);
		client.ConnectionLost += new ConnectionDelegate(OnConnectionLost);
		client.PublishArrived += new PublishArrivedDelegate(OnMessageArrived);
		Connect();
		InvokeRepeating("SendAliveMessage", 10,5);
	}
	
	protected void Connect()
	{   ClientConnection=true;
		try {
			client.Connect(true);
            
		} catch (System.Exception ex) {
			Debug.LogError("Failed to connect to " + Config.kConnectionString);
			Debug.LogException(ex);
            ClientConnection=false;
		}
	}
	IEnumerator Wait(){
        yield return new WaitForSeconds (1.5f);
        Connect();
    }
	
	public virtual void Update()
	{
		
		
			if(!client.IsConnected && ClientConnection==false)
                StartCoroutine("Wait");
		
	}
	
	void OnDisable()
	{
		client.Disconnect();
        ClientConnection=false;
	}
	
	#region Callbacks
	
	protected virtual void OnConnect(object sender, System.EventArgs e)
	{
		Debug.Log(clientId + " connected");
        ClientConnection=true;
	}
	
	protected virtual void OnConnectionLost(object sender, System.EventArgs e)
	{
		Debug.LogError(clientId + " connection lost");
        try
        {Connect();
        }
        catch
        {
            Debug.Log("ERR");
        }
        ClientConnection=false;
	}
	
	protected virtual bool OnMessageArrived(object sender, PublishArrivedArgs e)
	{
		Debug.Log(clientId + " received message: " + e.Payload.ToString());
		
		return true;
	}
	
	#endregion
}