﻿using UnityEngine;
using System.Collections;
using MqttLib;
using System;
using SimpleJSON;
using System.Collections.Generic;
using UnityEngine.UI;

public class MQTTScreenController : MQTTHandlerBase{

	
    override protected string clientId { get { return "DNA_Info"; } }
    public string lastTopic = "";
    public string lastMessage = "";
    
    public GameObject BreakCamera;
	public Image MainImage;
    public GameObject CircleWait;
    public Sprite DNA_INPUT;
    public Sprite DNA_ANALIZE;
    public Sprite DNA_VIRUS;
    public Sprite DNA_HEAL;
    public Sprite DNA_HEAL_COMPLETE;
    public GameObject video;
	
	public Sprite AfterVideoScreen;
	
	public enum Stages {input=0,analize,virus,heal,heal_complite};
    public Stages AppStage;
	
	
    
	
	
    
    
    public void SendState(string state){
		try {
			client.Publish(Config.kDNAStateTopic, state, QoS.OnceAndOnceOnly, false);
            	
		} catch (Exception ex) {
			client.Disconnect();
            
		}
	}
    
    void Start()
    {
        AppStage=Stages.input;
        //DisableAllSatge();

    }
    
    public virtual IEnumerator StartVideo(){
        yield return new WaitForSeconds(1);
        MainImage.sprite=AfterVideoScreen;
        video.SetActive(true);
        
       
   }
   public virtual IEnumerator StartVirusScene(){
        yield return new WaitForSeconds(6);
        lastTopic = Config.kDNAInfoScreensTopic;
        lastMessage="DNA_VIRUS";
        
       
   }
    override public void Update()
    {
        base.Update();
        
        if (lastTopic == Config.kDNAButtonsTopic) {
            
            if(lastMessage=="ENTER"){
                if(AppStage==Stages.virus){
                    lastTopic = Config.kDNAInfoScreensTopic;
                    lastMessage="DNA_HEAL";
                    
                }
                if(AppStage==Stages.heal){
                    StartCoroutine(StartVideo());
                    
                }
            }
            
                
            
            
            
        }
		
		
		if (lastTopic == Config.kDNAResetTopic)
		{
			if(lastMessage=="RESET")
			{
				Application.LoadLevel("Main");
			}
            if(lastMessage=="BREAK")
			{
				BreakCamera.SetActive(true);
			}
		}
		if (lastTopic == Config.kDNAInfoScreensTopic)
		{
		
            if(lastMessage=="DNA_INPUT"){
                MainImage.sprite=DNA_INPUT;
                AppStage=Stages.input;
            }
            if(lastMessage=="DNA_ANALIZE"){
                MainImage.sprite=DNA_ANALIZE;
                CircleWait.SetActive(true);
                StartCoroutine(StartVirusScene());
                AppStage=Stages.analize;
                
                
                
            }
            if(lastMessage=="DNA_VIRUS"){
                MainImage.sprite=DNA_VIRUS;
                AppStage=Stages.virus;
                
            }
			if(lastMessage=="DNA_HEAL"){
                MainImage.sprite=DNA_HEAL;
                //StartCoroutine(StartVideo());
                AppStage=Stages.heal;

                
                
            }
            if(lastMessage=="DNA_HEAL_COMPLETE"){
                MainImage.sprite=DNA_HEAL_COMPLETE;
                AppStage=Stages.heal_complite;
                
            }
        }
		if(lastTopic==Config.kDNALoadLevelTopic){
            if(lastMessage=="Level1"){
                Application.LoadLevel("level1");
            }
            if(lastMessage=="Level2"){
                Application.LoadLevel("level2");
            }
            if(lastMessage=="Level3"){
                Application.LoadLevel("level3");
            }
            
            if(lastMessage=="Level4"){
                Application.LoadLevel("level4");
            }
            if(lastMessage=="Level5"){
                Application.LoadLevel("level5");
            }
        }
        
        lastTopic = "";
        lastMessage = "";
            
   
    }
    public bool isMQTTConnect() {
        return client.IsConnected;
    }

        // событие успешного подключения
    override protected void OnConnect(object sender, EventArgs e)
    {
        base.OnConnect(sender, e);
        client.Subscribe(Config.kDNALoadLevelTopic, QoS.OnceAndOnceOnly);
        client.Subscribe(Config.kDNAResetTopic, QoS.BestEfforts);
        client.Subscribe(Config.kDNAButtonsTopic, QoS.BestEfforts);
		client.Subscribe(Config.kDNAInfoScreensTopic, QoS.BestEfforts);
        
		
		
		
		
    }

    // событие получение сообщения
    override protected bool OnMessageArrived(object sender, PublishArrivedArgs e)
    {
        base.OnMessageArrived(sender, e);
        bool result = base.OnMessageArrived(sender, e);
        lastTopic = e.Topic.ToString();
        lastMessage = e.Payload.ToString();

        return result;
    }

}

