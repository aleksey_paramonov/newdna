﻿using UnityEngine;
using System.Collections;
using MqttLib;
using System;
using SimpleJSON;
using System.Collections.Generic;
using UnityEngine.UI;

public class MQTTStageController : MQTTHandlerBase {

    override protected string clientId { get { return "DNA_GO1"; } }
    public string lastTopic = "";
    public string lastMessage = "";
    
    public GameObject BreakCamera;
    public CellGrid CellGrid;
    public Unit player;
    
    
	
	
     void KeyControll(Vector2 direction){
        player.AutoSelectUnit();
         var neighbour = CellGrid.Cells.Find(c => c.OffsetCoord == player.Cell.OffsetCoord + direction);
             if(neighbour != null){
                 neighbour.OnMouseDown();
             }
    }
     public void SendLevelState(string state){
		try {
			client.Publish(Config.kDNALevelStateTopic, state, QoS.OnceAndOnceOnly, false);
            	
		} catch (Exception ex) {
			client.Disconnect();
            
		}
	}
    public void SendState(string state){
		try {
			client.Publish(Config.kDNAStateTopic, state, QoS.OnceAndOnceOnly, false);
            	
		} catch (Exception ex) {
			client.Disconnect();
            
		}
	}
    
    void Start()
    {
        
        //DisableAllSatge();

    }
    
    void KeyController(string Key){
        
        if(Input.GetKeyDown(KeyCode.N) || Key=="NEXT")
        {
            CellGrid.EndTurn();//User ends his turn by pressing "n" on keyboard.
        }
        if(Input.GetKeyDown(KeyCode.UpArrow) || Key=="UP")
        {   
            Vector2 direction=new Vector2(0, 1);
            KeyControll(direction);
            //CellGrid.EndTurn();//User ends his turn by pressing "n" on keyboard.
        }
         if(Input.GetKeyDown(KeyCode.DownArrow) || Key=="DOWN")
        {   
            Vector2 direction=new Vector2(0, -1);
            KeyControll(direction);
            
        }
         if(Input.GetKeyDown(KeyCode.RightArrow) || Key=="RIGHT")
        {   
            Vector2 direction=new Vector2(1, 0);
            KeyControll(direction);
            //CellGrid.EndTurn();//User ends his turn by pressing "n" on keyboard.
        }
         if(Input.GetKeyDown(KeyCode.LeftArrow) || Key=="LEFT")
        {   
            Vector2 direction=new Vector2(-1, 0);
            KeyControll(direction);
        }
    }
    override public void Update()
    {
        base.Update();
        KeyController(lastTopic);
        if (lastTopic == Config.kDNAButtonsTopic) {
            KeyController(lastMessage);
            lastTopic = "";
            lastMessage = "";
        }
		
		
		if (lastTopic == Config.kDNAResetTopic)
		{
			if(lastMessage=="RESET")
			{
				Application.LoadLevel("Main");
			}
            if(lastMessage=="BREAK")
			{
				BreakCamera.SetActive(true);
			}
		}
        if(lastTopic==Config.kDNALoadLevelTopic){
            if(lastMessage=="Level1"){
                Application.LoadLevel("level1");
            }
            if(lastMessage=="Level2"){
                Application.LoadLevel("level2");
            }
            if(lastMessage=="Level3"){
                Application.LoadLevel("level3");
            }
            if(lastMessage=="Level4"){
                Application.LoadLevel("level4");
            }
            if(lastMessage=="Level5"){
                Application.LoadLevel("level5");
            }
        }

            
   
    }
    public bool isMQTTConnect() {
        return client.IsConnected;
    }

        // событие успешного подключения
    override protected void OnConnect(object sender, EventArgs e)
    {
        base.OnConnect(sender, e);
        client.Subscribe(Config.kDNALoadLevelTopic, QoS.OnceAndOnceOnly);
        client.Subscribe(Config.kDNAResetTopic, QoS.BestEfforts);
        client.Subscribe(Config.kDNAButtonsTopic, QoS.BestEfforts);
        
		
		
		
		
    }

    // событие получение сообщения
    override protected bool OnMessageArrived(object sender, PublishArrivedArgs e)
    {
        base.OnMessageArrived(sender, e);
        bool result = base.OnMessageArrived(sender, e);
        lastTopic = e.Topic.ToString();
        lastMessage = e.Payload.ToString();

        return result;
    }

}
