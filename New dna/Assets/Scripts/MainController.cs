﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class MainController : MonoBehaviour {
	
	public GameObject _gameOverPanel;
	public int MedKitCount=0;
	public MQTTStageController MQTT;
	public AchiveController Achives;
	private bool isAchiveCollected;
	// Use this for initialization
	void Start () {
		isAchiveCollected=false;
		try{
		Achives=GameObject.Find("Achives").GetComponent<AchiveController>();
		}
		catch (NullReferenceException ex){
		
			GameObject A= Instantiate(Resources.Load("Achives")) as GameObject;
			Achives=A.GetComponent<AchiveController>();	
		}
		//MQTT=GameObject.Find("MQTT").GetComponent<MQTTStageController>();
	
	}
	
	// Update is called once per frame
	void Update () {
			if(GetComponentInChildren<Animator>().GetCurrentAnimatorStateInfo(0).IsName("PlayerRepair") && isAchiveCollected==true)
				GetComponentInChildren<Animator>().SetBool("isRepair",false);
	
	}
	
    public string lastLevelName="level5";
    int maxAchivesCount=3;

    void OnTriggerEnter2D(Collider2D other) {
		//Debug.Log(other.gameObject.name);
		if(other.gameObject.tag == "Enemy"){
			//if(GetComponent<Unit>().unitDirection==other.gameObject.GetComponentInParent<Unit>().unitDirection){
				//other.gameObject.GetComponentInParent<GenericUnit>().KillUnit(GetComponent<Unit>());
				//Debug.Log("Kill");
				//}
			//else{
				other.gameObject.GetComponentInChildren<Animator>().SetBool("isAttack",true);
				GetComponentInChildren<Animator>().SetBool("isAttack",true);
				GetComponent<Unit>().OpositeDirection(other.gameObject.GetComponent<Unit>().unitDirection);
				if(isAchiveCollected){
					Achives.AchiveCount=Achives.AchiveCount-1;
					isAchiveCollected=false;
				}
				Debug.Log("LOSE"+other.gameObject.name);
				MQTT.SendLevelState(Application.loadedLevelName+":LOSE");
				//GameOver("LOSE");
				
				//Application.LoadLevel(Application.loadedLevelName);
			//}
		}
		if(other.gameObject.tag == "Block"){
			Debug.Log("Block");
			GetComponent<Unit>().StopMove();
			
			
		}
		if(other.gameObject.tag == "Exit")
		{
			Debug.Log("WIN");
			
			MQTT.SendLevelState(Application.loadedLevelName+":WIN");
			
			if(Application.loadedLevelName==lastLevelName){
				if(Achives.AchiveCount>0){
					GameOver("Wins with MedKit");
						if(Achives.AchiveCount>=maxAchivesCount)
							MQTT.SendState("WIN");
						if(Achives.AchiveCount<=(maxAchivesCount/2) && Achives.AchiveCount<=(maxAchivesCount/3) ){
							if(UnityEngine.Random.Range(0,2)==1){
								MQTT.SendState("WIN");
							}
							else{
								MQTT.SendState("LOSE");
							}
						}
						
				
				}
				else{
					MQTT.SendState("LOSE");
					}	
			Application.LoadLevel("Main");
			}
			else{
				Application.LoadLevel(Application.loadedLevel+1);
			}
			
		}
		if(other.gameObject.tag == "MedKit")
		{	isAchiveCollected=true;
			other.gameObject.SetActive(false);
			GetComponentInChildren<Animator>().SetBool("isRepair",true);
		

			Achives.AchiveCount++;
		}
        
    }
	void GameOver(string result){
		
		_gameOverPanel.SetActive(true);
        _gameOverPanel.transform.Find("InfoText").GetComponent<Text>().text = "Player "+result;
        
        _gameOverPanel.transform.Find("DismissButton").GetComponent<Button>().onClick.AddListener(DismissPanel);
 
        

	}
	public void DismissPanel()
    {
       _gameOverPanel.SetActive(false);
    }
}
