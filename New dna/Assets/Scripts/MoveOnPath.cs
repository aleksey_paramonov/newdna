﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Simple implementation of AI for the game.
/// </summary>
public class MoveOnPath : Player
{
    private CellGrid _cellGrid;
    private System.Random _rnd;
    
    public List<Cell> Mpath;
    public int startPoint=0;
    public List<Cell> path;
    public List<Cell> Testpath;
    public bool Inverse;
    public Unit unit;
    
    
    //public Cell StartCell;
    void Start(){
        //Mpath.Reverse();
        //path=new List<Cell>(Mpath.GetRange(startPoint,Mpath.Count-1));
        path=new List<Cell>(Mpath);
    }
    public MoveOnPath()
    {
        _rnd = new System.Random();
        
        
        
    }

    public override void Play(CellGrid cellGrid)
    {
        cellGrid.CellGridState = new CellGridStateAiTurn(cellGrid);
        _cellGrid = cellGrid;
		
        StartCoroutine(Play()); //Coroutine is necessary to allow Unity to run updates on other objects (like UI).
                                //Implementing this with threads would require a lot of modifications in other classes, as Unity API is not thread safe.
    }
    private IEnumerator Play()
    {
        
                if(path.Count==0){
                    if(Inverse)
                        Mpath.Reverse();
                    
                    path=new List<Cell>(Mpath);
                }
                List<Cell>CurrentPath=new List<Cell>();
                
                CurrentPath.Add(path[0]);
                
                
                unit.MyMove(path[0], CurrentPath);
                path.Remove(path[0]);
                while (unit.isMoving)
                        yield return 0;
                if(path.Count>0 && !unit.gameObject.GetComponentInChildren<Animator>().GetBool("isAttack"))
                    unit.ShowNextDirection(path[0]);
 
                
        //If the path cost is greater than unit movement points, move as far as possible.
         
        _cellGrid.EndTurn(); 
        //yield return 0;    
    }
}