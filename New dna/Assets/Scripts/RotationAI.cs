﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Simple implementation of AI for the game.
/// </summary>
public class RotationAI : Player
{
    private CellGrid _cellGrid;
    private System.Random _rnd;
	public Unit unit;
	public enum RotationDir {CW,CCW};
	public RotationDir myRotationDir;
	
    
  
    
    
    //public Cell StartCell;
    void Start(){
        
    }
    public RotationAI()
    {
        _rnd = new System.Random();
        
        
        
    }

    public override void Play(CellGrid cellGrid)
    {
        cellGrid.CellGridState = new CellGridStateAiTurn(cellGrid);
        _cellGrid = cellGrid;
		
        StartCoroutine(Play()); //Coroutine is necessary to allow Unity to run updates on other objects (like UI).
                                //Implementing this with threads would require a lot of modifications in other classes, as Unity API is not thread safe.
    }
    private IEnumerator Play()
    {	
		if(myRotationDir==RotationDir.CW)
        	unit.NextRotateCW();
		else{
			unit.NextRotateCCW();
		}
		// while (unit.isMoving)
        //                 
        _cellGrid.EndTurn();
		yield return 0;
		
		
          
    }
}