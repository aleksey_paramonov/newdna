﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VideoEnd : MonoBehaviour {
	public GameObject MainImage;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(GetComponent<MoviePlayer>().videoEnd){
			GetComponent<MoviePlayer>().videoEnd=false;
			
			MainImage.GetComponent<Animator>().SetBool("VideoEnd",true);
			gameObject.SetActive(false);
			
		}
	
	}
}
