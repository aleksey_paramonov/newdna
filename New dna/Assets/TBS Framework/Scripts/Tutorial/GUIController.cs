﻿using UnityEngine;

public class GUIController : MonoBehaviour
{
    public CellGrid CellGrid;
    public Unit player;
	
    void Start()
    {
        Debug.Log("Press 'n' to end turn");
    }
    void KeyControll(Vector2 direction){
        player.AutoSelectUnit();
         var neighbour = CellGrid.Cells.Find(c => c.OffsetCoord == player.Cell.OffsetCoord + direction);
             if(neighbour != null){
                 neighbour.OnMouseDown();
             }
    }
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.N))
        {
            CellGrid.EndTurn();//User ends his turn by pressing "n" on keyboard.
        }
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {   
            Vector2 direction=new Vector2(0, 1);
            KeyControll(direction);
            //CellGrid.EndTurn();//User ends his turn by pressing "n" on keyboard.
        }
         if(Input.GetKeyDown(KeyCode.DownArrow))
        {   
            Vector2 direction=new Vector2(0, -1);
            KeyControll(direction);
            
        }
         if(Input.GetKeyDown(KeyCode.RightArrow))
        {   
            Vector2 direction=new Vector2(1, 0);
            KeyControll(direction);
            //CellGrid.EndTurn();//User ends his turn by pressing "n" on keyboard.
        }
         if(Input.GetKeyDown(KeyCode.LeftArrow))
        {   
            Vector2 direction=new Vector2(-1, 0);
            KeyControll(direction);
        }
	}
}
